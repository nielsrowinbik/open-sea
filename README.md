# openSEA - The first open socio-environmental auditing tool

We've just moved over to Bitbucket from Github. The README will be updated accordingly at some point in the future.

### License

This project is licensed under the [GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html).
