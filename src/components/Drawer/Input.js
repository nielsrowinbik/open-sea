import styled from 'styled-components';

const Input = styled.input`
	width: 100%;
    height: 40px;
    border: none;
    font-family: inherit;
    font-size: 1.42857rem;
	padding: 0;
`;

export default Input;