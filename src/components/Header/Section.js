import styled from 'styled-components';

const Section = styled.div`
	flex: auto;
`;

export default Section;