import actions from './actions';

const ReportsStore = (state, initial) => actions(state);

export default ReportsStore;